"""
EXIF attributes to txt

Example:
python exif_reader.py bg3.jpg

The above line creates bg3.jpg_EXIF.txt with available EXIF data
"""
#pip -u install exif

import sys
import os
from pprint import pprint
from exif import Image
from pathlib import Path

def get_all_exif(img):
    """
    creates and returns dict from exif data
    """
    EXIF_dict = {} 
    for attribute in img.list_all():
        try:
            EXIF_dict[attribute] = img.get(attribute)
        except (KeyError, AttributeError, ValueError) as err:
            continue
            print(err)
    return EXIF_dict

if __name__ == "__main__":

    IN_DIR = os.getcwd()
    path = Path(os.path.join(IN_DIR, sys.argv[1]))
    with open(path, "rb") as img:
        img = Image(img)
		
    if img.has_exif:
        attr_dict = get_all_exif(img)
        with open(os.path.join(IN_DIR,f"{sys.argv[1]}_EXIF.txt"),"w") as ofile:
            ofile.write(f"{sys.argv[1]} EXIF: \n")
            ofile.write("\n")
            for k, v in attr_dict.items():
                ofile.write(f"{k}: ")
                ofile.write(f"{v}\n")
    else:
        print("No EXIF")

